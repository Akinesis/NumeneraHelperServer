//Required imports
const Express = require("express")
const session = require("express-session")
const BodyParser = require("body-parser")
const MongoClient = require("mongodb").MongoClient
const ObjectId = require("mongodb").ObjectID
const Crypto = require('crypto')

//DB url and DB name
const CONNECTION_URL = "thefoxden.hd.free.fr:27017"
const DATABASE_NAME = "numenera_helper"
const MONGO_PREFIX = 'mongodb://'
const DB_USERNAME = 'apiService'
const DB_PASSWORD = 'Thi$Ap|P0TecT'
 
//The application
let app = Express()
app.use(BodyParser.json())
app.use(BodyParser.urlencoded({ extended: true }))
app.use(session({
    secret: 'veryimportantsecret', 
    // Forces the session to be saved 
    // back to the session store 
    resave: true, 
  
    // Forces a session that is "uninitialized" 
    // to be saved to the store 
    saveUninitialized: true
}))

//Database reference
let database
let personnagesCollection, cyphersCollection
 
app.listen(1337, () => {
	//Try to connect with DB
	
	let constructedUrl = MONGO_PREFIX+DB_USERNAME+':'+DB_PASSWORD+'@'+CONNECTION_URL
	//let constructedUrl = MONGO_PREFIX+CONNECTION_URL
	
	
    MongoClient.connect(constructedUrl, { useNewUrlParser: true, useUnifiedTopology: true }, (error, client) => {
        if(error) {
            throw error;
        }
		
		//If success, save db reference and collections reference
        database = client.db(DATABASE_NAME)
        personnagesCollection = database.collection("personnages")
		cyphersCollection = database.collection("cyphers")
		
		//Notify connexion success
        console.log("Connected to `" + DATABASE_NAME + "`!")
    })
})

app.post("/loggin", (request, response) => {
	
	let characterName = request.body.characterName
	 
    personnagesCollection.findOne({ "nom": characterName }, (error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
		if(result == null){
			return response.status(401).send();
		}
		
		let authToken = Crypto.randomBytes(15)
								.toString('base64')
								.slice(0, 15)
			
		let fullResponse = {
			authToken : authToken,
			character : result
		}
		
		request.session.name = characterName
		request.session.authToken = authToken
		
        response.send(fullResponse)
    })
})

app.get("/testSession",(request, response) =>{

	console.log(request.session.name)

})

//Insert one character into the collection "personnages"
app.post("/personages", (request, response) => {
	
	//Try to insert into DB
    personnagesCollection.insertOne(request.body, (error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
		//If success, send DB result
		let reqResult = result.result
		reqResult.insertedId = result.insertedId
		
        response.send(reqResult);
    })
})

//Get all the characters allowed to be showned
 app.get("/personages", (request, response) => {
    personnagesCollection.find({ showInList :{$not : {$eq : false }}}).toArray((error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
        response.send(result);
    });
})

//Get a specific character by it's name
app.get("/personages/:nomLogin", (request, response) => {
	
    personnagesCollection.findOne({ "nom": request.params.nomLogin }, (error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
		if(result == null){
			return response.status(401).send();
		}
        response.send(result)
    })
})

//Get x random cypher
app.get("/cyphers/random/:number", (request, response) => {
	
	let numberOfCyphers = parseInt(request.params.number)
	
    cyphersCollection.aggregate([{$sample: {size : numberOfCyphers}}]).toArray((error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
		let returnResult = {cyphers:result}
        return response.status(200).send(returnResult)
    });
})

//Get a random untranslated cypher
app.get("/cyphers/randomUntranslated", (request, response) => {

	//Get one random cypher with no "FR" field
    cyphersCollection.aggregate([{$match: {"FR": {$exists: false}}},
								{$sample: {size : 1}}])
	.toArray((error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
		let returnResult = {cyphers:result}
        return response.status(200).send(returnResult)
    });
})

//Add translation to a cypher
app.put('/cyphers/translate',(request, response) => {
	let cypherInfo = request.body

	console.log(cypherInfo.cypherUId)

	cyphersCollection.findOne(ObjectId(cypherInfo.cypherUId), (error, result) => {

		if(error) {
            return response.status(500).send(error);
        }
		if(result == null){
			return response.status(401).send();
		}

		result['FR']= {'title':cypherInfo.title, 'text':cypherInfo.text}

		console.log(result)
	})

})

/**
 * Add a cypher to a character.
 * @param {Json} request
 * @param {Json} response 
 */
app.put('/personages/:nomLogin/addCypher',(request, response) => {

	let characterName = request.params.nomLogin
	let cyphers = request.body.cyphers
	
	cyphers.forEach(element => element._id = new ObjectId() )
	
	personnagesCollection.findOne({ "nom": characterName }, (error, result) => {
        if(error) {
            return response.status(500).send(error);
        }
		if(result == null){
			return response.status(401).send();
		}
		
		let charId = result._id
		
		result.cyphers = result.cyphers.concat(cyphers);
		
		//If this function is called to keep track of used cyphers
		if (characterName == "Hisorique"){
			//only keep in history the last 15 cyphers
			while(result.cyphers.length > 15){
				result.cyphers.shift()
			}
		}
		
		personnagesCollection.updateOne({ "_id": charId }, {$set: {"cyphers":result.cyphers}},{ upsert: true },(error, result2) => {
			
			if(error) {
				return response.status(500).send(error);
			}
			
			return response.status(200).send()
		})
    })

})

//Remove a cypher from a character
app.delete('/personages/:nomLogin/removeCypher',(request, response) => {
	let characterName = request.params.nomLogin
	let cypher = request.body
	
	if(request.session.authToken == request.get('authToken')){
		
		personnagesCollection.findOne({ "nom": characterName }, (error, result) => {
			if(error) {
				return response.status(500).send(error);
			}
			if(result == null){
				return response.status(401).send();
			}
			
			let charId = result._id
			let cypherArray = result.cyphers
			
			for (var i = cypherArray.length; i--; ) {
				if (cypherArray[i]._id == cypher._id) {
					cypherArray.splice(i, 1)
				}
			}

			result.cyphers = cypherArray
			
			personnagesCollection.updateOne({ "_id": charId }, {$set: {"cyphers":result.cyphers}},{ upsert: true },(error, result2) => {
				
				if(error) {
					return response.status(500).send(error);
				}
				
				personnagesCollection.findOne({ "nom": characterName }, (error, updatedCharacter) => {
					if(error) {
						return response.status(500).send(error);
					}
					
					//Return the updated character
					return response.status(200).send(updatedCharacter)
				})

			})
		})
		
	}else{
		console.log('Bad auth token : ' + request.session.authToken +" / "+ request.get('authToken'))
		return response.status(401).send()
	}

})

/*==========================
	   Fonctions de test
============================*/

//Get 10 cyphers
app.get("/cyphers/getTen", (request, response) => {
    cyphersCollection.find({},{limit:10}).toArray((error, result) => {
        if(error) {
            return response.status(500).send(error);
        }

        response.send(result);
    });
})

exports.app = app