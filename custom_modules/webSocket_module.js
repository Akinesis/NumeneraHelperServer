const WebSocket = require('ws')
const ADMIN_NAME = "Admin"

const REACTION = { C:"est en colère", P:"as peur", D:"Est d'accord", T:"Veux trahir", V:"Veux voler un truc", K:"veux fair un meurtre"}
const ACTION = { V:"veux voler un truc", T:"veux tuer"}
const MSG_TYPE = { R:REACTION, A:ACTION}


const ws = new WebSocket.Server({ 
	port: 2201,
	noServer: true},
	()=>{
		console.log('server started')
})

let activeUsers = {}
let adminUser = null

ws.on('connection', function connection(ws, request) {

   ws.on('message', (messageContent) => {
	  
	  let parsedMsg = JSON.parse(messageContent)
	  
	  if(parsedMsg.firstConnect){
		activeUsers[parsedMsg.name] = ws
		if(parsedMsg.name == ADMIN_NAME){
			console.log("Admin logged in !")
			adminUser = ws
		}
		console.log("Logged : " + parsedMsg.name)
	  }else{
		  //if the admin is logged in
		  if(adminUser != null){
			computeMessage(parsedMsg)
		  }
	  }
   })
})

ws.on('listening',()=>{
   console.log('listening on 2201')
})

function computeMessage(parsedMsg){
	let splitMsg = parsedMsg.message.split(":")
	
	let mesageToSend = parsedMsg.sender + " " + MSG_TYPE[splitMsg[0]][splitMsg[1]]
	
	adminUser.send(mesageToSend)
}

exports.webSocket = ws