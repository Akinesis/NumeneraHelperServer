const express = require('express');
let router = express.Router();
const http = require('http')

const OPTIONS_RANDOM_CYPHER = {
  hostname: 'thefoxden.hd.free.fr',
  port: 1337,
  path: '/cyphers/randomUntranslated',
  method: 'GET'
}

const OPTIONS_INSERT_TRANSLATION = {
  hostname: 'thefoxden.hd.free.fr',
  port: 1337,
  path: '/cyphers/translate',
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json',
  }
}

//page variables
let cyperName = 'Cypher name'
let cypherText = 'Cypher description'
let cypherUid = 0

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { cypherName: cyperName, cypherText: cypherText, cypherUid: cypherUid })
})

router.post('/translate', function(req, mainRes, next) {
  //res.render('layout', { title: 'Express' });
  let reqBody = req.body;
  //console.log(reqBody)

  let data = JSON.stringify(reqBody)

  OPTIONS_INSERT_TRANSLATION.headers['Content-Length']=data.length

  const putReq = http.request(OPTIONS_INSERT_TRANSLATION, res => {
    console.log(`statusCode: ${res.statusCode}`)
  
    res.on('data', d => {
      process.stdout.write(d)
    })
  })
  
  putReq.on('error', error => {
    console.error(error)
  })
  
  putReq.write(data)
  putReq.end()

  mainRes.redirect('/')
})

//Called when "random cypher" is hit
router.get('/random', function(req, mainRes, next) {

  //Get a random untranslated cypher from db
  const randomCypherReq = http.request(OPTIONS_RANDOM_CYPHER, res => {
    console.log(`statusCode: ${res.statusCode}`)
  
    res.on('data', d => {

      let cypher = JSON.parse(d).cyphers[0]

      //Render the page with the cypher name and text
      cyperName = cypher.title
      cypherText = cypher.text
      cypherUid = cypher._id
      mainRes.end()
    })
  })

  randomCypherReq.on('error', error => {
    console.error(error)
  })
  
  randomCypherReq.end()

  //res.json({ cypherName: cyperName, cypherText:cypherText })
})


module.exports = router;
