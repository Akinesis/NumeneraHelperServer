//Required imports
const path = require('path')
const express = require('express')

//Application page link
const indexRouter = require('./routes/index.js')
const usersRouter = require('./routes/users.js')

//The webSocket module
const WebSocket = require("./custom_modules/webSocket_module.js").webSocket
 
//Set the api application and get it back
let app = require("./custom_modules/dbModule").app

// view engine setup
app.set('views', path.join(__dirname, 'public/views'))
app.set('view engine', 'pug')
app.use(express.static('public'))

app.use(express.json())
app.use(express.urlencoded({ extended: false }))

//Configure html root of the app
app.use('/', indexRouter)
app.use('/users', usersRouter)